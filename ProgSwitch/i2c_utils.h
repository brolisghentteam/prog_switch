


#ifndef I2C_UTILS_H
#define I2C_UTILS_H

#define I2C_TOUT	2	/* in ms */

#define I2C_ERR_BUSY		1
#define I2C_ERR_ADDR_TOUT	2
#define I2C_ERR_DATA_TOUT	3
#define I2C_ERR_REG_TOUT	4
#define I2C_OK				0


/* ============================ i2c communication functions ===================== */

extern uint8_t i2c_ack_wait(void);
extern uint8_t i2c_bus_wait(void);
extern int8_t i2c_reg_read(uint8_t addr, uint8_t reg, uint8_t *data, uint8_t count);
extern int8_t i2c_write(uint8_t addr, uint8_t *data, uint8_t count);
extern void init_i2c(void);

#endif //I2C_UTILS_H