
#include <stdint.h>
#include <util/delay.h>
#include <avr/io.h>
#include "i2c_utils.h"


/* ============================ i2c communication functions ===================== */

uint8_t i2c_ack_wait(void) {

	uint16_t i = 0;


	while ((TWIC.MASTER.STATUS & 0x70) != 0x60) {	// wait for ack.
		_delay_us(10);
		if (i++ > (I2C_TOUT * 100)) {
			return 1;
		}
	}
	return 0;
}


uint8_t i2c_bus_wait(void) {

	uint16_t i = 0;


	while ((TWIC.MASTER.STATUS & 0x03) > 2) {	// wait for i2c bus
		_delay_us(10);
		if (i++ > (I2C_TOUT * 100)) {
			return 1;
		}
	}
	return 0;
}


int8_t i2c_reg_read(uint8_t addr, uint8_t reg, uint8_t *data, uint8_t count) {

	uint16_t i = 0;
	uint8_t j = 0;


	if (i2c_bus_wait()) {
		return I2C_ERR_BUSY;
	}

	TWIC.MASTER.ADDR = ((addr << 1) & 0xFE);

	if (i2c_ack_wait()) {
		return I2C_ERR_ADDR_TOUT;
	}

	TWIC.MASTER.DATA = reg;

	if (i2c_ack_wait()) {
		return I2C_ERR_REG_TOUT;
	}

	TWIC.MASTER.ADDR = (((addr << 1) & 0xFE) | 0x01);

	for (j = 0; j < count; j++) {
		i = 0;
		while ((TWIC.MASTER.STATUS & 0xB0) != 0xA0) {	// wait for read response
			_delay_us(10);
			i++;
			if (i > (I2C_TOUT * 100)) {
				return I2C_ERR_DATA_TOUT;
			}
		}

		data[j] = TWIC.MASTER.DATA;
		if (j < count - 1) {
			TWIC.MASTER.CTRLC = 0x02;	// ACK and READ
		}
	}
	TWIC.MASTER.CTRLC = 0x07;	// NACK and STOP

	return I2C_OK;
}


int8_t i2c_write(uint8_t addr, uint8_t *data, uint8_t count) {

	uint8_t i;


	if (i2c_bus_wait()) {
		return I2C_ERR_BUSY;
	}

	TWIC.MASTER.ADDR = ((addr << 1) & 0xFE);

	if (i2c_ack_wait()) {
		return I2C_ERR_ADDR_TOUT;
	}

	for (i = 0; i < count; i++) {
		TWIC.MASTER.DATA = data[i];

		if (i2c_ack_wait()) {
			return I2C_ERR_DATA_TOUT;
		}
	}
	TWIC.MASTER.CTRLC = 0x07;
	return I2C_OK;
}

void init_i2c()
{
	TWIC.MASTER.BAUD = 55;		/* 155 for 100 kHz		*/
	TWIC.MASTER.CTRLB = 0x0C;	/* bus timeout to 200 us	*/
	TWIC.MASTER.CTRLA = 0x08;	/* enable TWI master		*/
}