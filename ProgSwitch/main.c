#include <atmel_start.h>
#include <stdio.h>
#include <stdint.h>
#include <util/delay.h>
#include <avr/io.h>

#include "i2c_utils.h"
#include "uart_utils.h"


static inline void set_bit(volatile uint8_t *x, uint8_t bitNum) {
	*x |= (1 << bitNum);
}

static inline void clear_bit(volatile uint8_t *x, uint8_t bitNum) {
	*x &= (1 << bitNum);
}


#define N_O_EXPANDERS sizeof expander_address
const uint8_t expander_address[] = {0x20, 0x21, 0x22, 0x23};
uint8_t expander_port_address[] = {0x02, 0x03};
uint8_t expander_port_cfg_register[] = {0x06, 0x07};



#define STX '$'
#define STR '^'
#define ERR 0x88
#define PONG

#define CMD_PING 0x01
#define CMD_RESET 0x02
#define CMD_SET_MODE 0x03
#define CMD_SET_OUTPUT 0x04
#define CMD_CLEAR 0x05

#define RESP_OK 0x0A

#define ERR_INV_CMD 0x81
#define ERR_INV_ARG 0x82
#define ERR_INV_LEN 0x83
#define ERR_INV_CSUM 0x84


enum mode {normal, ext};	
mode = normal;




/* IO macros */
#define YELLOW_LED_ON()		PORTD.OUT &= ~0x10
#define YELLOW_LED_OFF()	PORTD.OUT |= 0x10
#define YELLOW_LED_TGL()	PORTD.OUTTGL = 0x10
#define ORANGE_LED_ON()		PORTD.OUT &= ~0x08
#define ORANGE_LED_OFF()	PORTD.OUT |= 0x08
#define ORANGE_LED_TGL()	PORTD.OUTTGL = 0x08

#define MODE_ON()			PORTC.OUT |= 0x20
#define MODE_OFF()			PORTC.OUT &= ~0x20





static uint8_t calculate_csum(uint8_t cmd, uint8_t n_o_args, uint8_t args[])
{
	
	uint8_t csum = cmd;
	
	csum += n_o_args & 0xFF;
	for (int i=0; i < n_o_args; i++)
		csum += args[i];
	return (~csum + 1) & 0xFF;
	
}

void set_mode(uint8_t n_o_args, uint8_t *args)
{
	
	if (n_o_args != 1)
	{
		put_char(ERR_INV_LEN);
		return;
	}
	
	switch (args[0])	
	{
		case 1:
			mode = normal;
			put_char(RESP_OK);
			break;
		
		case 2:
			mode = ext;
			put_char(RESP_OK);
			break;
		
		default:
			put_char(ERR_INV_ARG);
	}
		
}

void set_output(uint8_t n_o_args, uint8_t *args)
{
	/* expected 3 bytes arguments: [DEVICE, PORT, DATA] */
	
	uint8_t write_data[2];
	uint8_t device_address;
	uint8_t resp;
	
	if (n_o_args != 3)
	{
		put_char(ERR_INV_LEN);
		return;
	}
	
	device_address = expander_address[args[0]];
	
	write_data[0] = expander_port_address[args[1]];
	write_data[1] = args[2];
	
	resp = i2c_write(device_address, write_data, 2);
	
	if (resp == I2C_OK)
		put_char(RESP_OK);
	else
		put_char(ERR);
	
}


void reset_switch()
{
	
	mode = normal;
	init_i2c_expanders();
	
}



void interpret_command(uint8_t cmd, uint8_t n_o_args, uint8_t args[])
{
	switch (cmd)
	{
		case CMD_PING:			
			put_char(RESP_OK);
			break;
		
		case CMD_SET_OUTPUT:
			set_output(n_o_args, args);
			break;
		
		case CMD_SET_MODE:
			set_mode(n_o_args, args);
			break;
		
		case CMD_RESET:
			reset_switch();
			put_char(RESP_OK);
			break;
		
		case CMD_CLEAR:
			init_i2c_expanders();
			put_char(RESP_OK);
			break;		
		
		default:
			put_char(ERR_INV_CMD);
	}	
	
	
}



void receive_command()
{
	uint8_t cmd;
	uint8_t n_o_args;
	uint8_t args[10];
	uint8_t csum;
	
	cmd = get_char();
	n_o_args = get_char();
	
	for (int i=0; i < n_o_args; i++)
	{
		args[i] = get_char();
	}
	
	csum = get_char();
	
	if (csum != calculate_csum(cmd, n_o_args, args))
	{
		put_char(ERR_INV_CSUM);
		return;
	}
			
	interpret_command(cmd, n_o_args, args);
	put_char(STR);
}



void init_i2c_expanders()
{			
		
	const uint8_t addrs[] = {0x20, 0x21, 0x22, 0x23};
	int8_t res;
	uint8_t i;
	
	res = 0;
	for (i = 0; i < 4; i++) {
		/* set output to 0 */
		res += i2c_write(addrs[i], "\x02\x00", 2);
		res += i2c_write(addrs[i], "\x03\x00", 2);
		/* set as output */
		res += i2c_write(addrs[i], "\x06\x00", 2);
		res += i2c_write(addrs[i], "\x07\x00", 2);
	}
	/* res=0 means that everything is OK */
	
	if (res != 0)
	{
		ORANGE_LED_ON();
		while(1);
	}
	
}






int main(void)
{
		
	char recv_char;	
	
	
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	/* Configure Port B for output. */
	PORTB.DIRSET = 0xFF;
	
	PORTD.OUT = 0x18;
	PORTD.DIR = 0x18;
	
	PORTC.OUT = 0x00;
	PORTC.DIR = 0x20;
	
	init_uart();
	YELLOW_LED_ON();
	init_i2c();
	YELLOW_LED_OFF();
	_delay_ms(1500);
	YELLOW_LED_ON();
	init_i2c_expanders();
	YELLOW_LED_OFF();
	
	MODE_OFF();
	
	
	while (1) {					
					
		recv_char = get_char();
		
		if (recv_char == STX)
		{			
			ORANGE_LED_OFF();
			YELLOW_LED_ON();
			receive_command();
			YELLOW_LED_OFF();			
			
		}
		else
		{
			ORANGE_LED_ON();
		}		
				
	}
		
	
	
}
